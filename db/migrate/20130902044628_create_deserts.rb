class CreateDeserts < ActiveRecord::Migration
  def change
    create_table :deserts do |t|
      t.string :name
      t.text :recipe
      t.text :instruction
      t.string :img_url
      t.integer :chef_id

      t.timestamps
    end
  end
end
