json.array!(@chefs) do |chef|
  json.extract! chef, :name
  json.url chef_url(chef, format: :json)
end
