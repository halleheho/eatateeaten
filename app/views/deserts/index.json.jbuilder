json.array!(@deserts) do |desert|
  json.extract! desert, :name, :recipe, :instruction, :img_url, :chef_id
  json.url desert_url(desert, format: :json)
end
