class Chef < ActiveRecord::Base
	validates :name, presence: true
	has_many :deserts

end
