class Desert < ActiveRecord::Base
	belongs_to :chef
	validates :name, presence: true
	validates :recipe, presence: true
	validates :instruction, presence: true
	validates :img_url, presence: true
	
end
